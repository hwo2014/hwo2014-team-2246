(function()
{
  console.log('mode.<init>');

  var config = 
  {
    behaviour: 'normal',
    carCount: 1,
    track: null,
    password: null
  };

  module.exports = 
  {
    getConfig: function() { return config; },
    setConfig: function(c) { config = c; },
    setTrack: function(t) { config.track = t; }
  };

})();

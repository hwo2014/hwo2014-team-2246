(function()
{ 
  console.log('newpos.<init>');
  
  var model = require('./model');
  var linear = require('./linear');
  var track = require('./track');
  
  function next(send)
  {  
    var numLaps = track.getNumLaps() || 0;
    var last = model.last();
    var current = model.current();
    var currentPiece = current.piece;
    var nextPiece = currentPiece.next;    
    var angle = Math.abs(current.angle);
    
    // slipping?
    var slipping = (Math.abs(angle) - Math.abs(last.angle)) > 0;
    
    if (currentPiece.isHomeStraight)
    {
      if (currentPiece !== last.piece)
      {
        send({ msgType:'turbo', data:'zoom' });
        current.behaviour = '*** turbo ***';
        return 1;
      }
      else
      {
        current.behaviour = 'home straight';
        return 1;
      }
    }    
    else if (currentPiece.isTurn)
    {    
      var max = currentPiece.turn.lanes[current.lane].maxSpeed;

      if (!nextPiece.isTurn || 
          (currentPiece.angle > 0 && nextPiece.angle < 0) ||
          (currentPiece.angle < 0 && nextPiece.angle > 0))
      {
        if (angle < 55 && !slipping)
        {
          // coming out of a turn or changing direction - increase speed 
          // TODO could make angle check dynamic by checking angular velocity?
          
          var factor = currentPiece.turn.fastExit || 1.30; 
          
          max *= factor; 
        }
      }

      // at the moment don't calculate braking distance to next turn
      // perhaps should, so instead just take it's speed into account...
      var nextTurn = currentPiece.next.turn;
      if (nextTurn)
      {
        max = Math.min(max, nextTurn.lanes[current.lane].maxSpeed); 
      }
      
      var curr = current.speed;
      
      current.behaviour = 'turn '+currentPiece.turn.index+' speed '+max.toFixed(3)+' '+withSign(curr-max)+' slip='+(slipping?'YES':'x');
      
      var throttle = model.getThrottleForSpeed(max);
      return smooth(last.throttle,throttle);
    }
    else
    {
      // calculate available distance to next turn
      // and the required distance to brake to the turn's max speed
    
      var available = currentPiece.lane[current.lane].length - current.pos;;      
      var nextTurnPiece = nextPiece;
      while (!nextTurnPiece.isTurn) 
      {
        available += nextTurnPiece.lane[current.lane].length;
        nextTurnPiece = nextTurnPiece.next;
      }
      
      var throttle = 1;
      var nextMaxSpeed = nextTurnPiece.turn.lanes[current.lane].maxSpeed;
      
      // purposely go into into turns too fast and slow down in them
      // as this is a better slip curve
      var factor = nextTurnPiece.turn.fastEntry || 1.15; 
      nextMaxSpeed = nextMaxSpeed * factor;

      if (nextTurnPiece.index < currentPiece.index && current.lap == numLaps-1)
      {
        // last lap final straight - no need to brake!
        current.behaviour = 'last straight';
        return 1;
      }
      else if (current.speed < nextMaxSpeed)
      {
        current.behaviour = 'under next turn '+nextTurnPiece.turn.index+' speed '+nextMaxSpeed.toFixed(3);
        throttle = model.getThrottleForSpeed(nextMaxSpeed);
      }
      else
      {
        var maxSpeed = nextMaxSpeed;
        var required = linear.brakingDistance(current.speed, maxSpeed);

        // note that if we don't brake then we have to assume we'll
        // move an extra speed+acceleration distance, so make sure we have
        // room for that otherwise we'll never be able to brake in time
        available -= (current.speed + current.acceleration + 1);
        
        var excess = available - required;

        current.behaviour = 'brake distance '+withSign(excess)+' for next turn '+nextTurnPiece.turn.index+' speed '+maxSpeed.toFixed(3);

        throttle = excess>0 ? 1 : 0; 
      }    
      
      return throttle;
    }
  }
  
  function smooth(a,b)
  {
    if (a === null || a === undefined) a = 0;
    if (b === null || b === undefined) b = 0;
    return a/4.0 + 3.0*b/4.0;
  }
  
  function withSign(num)
  {
    return (num > 0) ?  '+'+num.toFixed(3) : num.toFixed(3);
  }

  module.exports = 
  {
    next: next,
  };
    
})();






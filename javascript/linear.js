(function()
{
  console.log('linear.<init>');
  
  var constants = require('./model').constants();

  var ZERO = { pos: 0 };

  function accelerate(from,to)
  {
    if (isNaN(from) || isNaN(to)) return ZERO;

    var pos   = 0;
    var speed = from;
    var ticks = 0;
    var gas   = from > to ? 0 : 1;
  
    while (ticks++ < 2000)
    {
      if (from >= to && speed <= to) return { type:'-', from:from.toFixed(4), to:to.toFixed(4), ticks:ticks, pos:pos.toFixed(4) };
      if (from <  to && speed >= to) return { type:'+', from:from.toFixed(4), to:to.toFixed(4), ticks:ticks, pos:pos.toFixed(4) };
      var acceleration = constants.A*gas - constants.B*speed;
      speed += acceleration;
      pos += speed;
    }
  
    //console.log('*** ERROR: out of iterations *** from='+from+' to='+to);
    return ZERO;
  }
  
  exports.brakingDistance = function(from,to)
  {
    return accelerate(from,to).pos;
  };

})();
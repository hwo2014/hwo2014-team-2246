(function()
{ 
  console.log('switch.<init>');
  
  var model = require('./model');
  var track = require('./track');
  
  function next(send)
  {  
    var last    = model.last();
    var current = model.current();
    var cars    = model.cars();
  
    if (last.piece === current.piece) return;  
    if (!current.piece.next.isSwitch) return;
    
    // note assumes indexes go left to right (distance from center -ve to +ve)
    
    var nextSegment  = current.piece.next.segment;
    var maxLaneIndex = track.getMaxLaneIndex();
    var laneIndex    = current.lane;      
    
    var left    = laneIndex === 0            ? 99999 : nextSegment[laneIndex-1];
    var current =                                      nextSegment[laneIndex];
    var right   = laneIndex === maxLaneIndex ? 99999 : nextSegment[laneIndex+1];
    
    if (left < right)
    {
      if (left < current || (cars.blocked && Math.random()>=0.5)) // sophisticated overtaking logic :-)
      {
        send({ msgType:"switchLane", data:"Left" });
      }
    }
    else
    {
      if (right < current || (cars.blocked && Math.random()>=0.5))
      {
        send({ msgType:"switchLane", data:"Right" });
      }
    }
  }

  function random(send)
  {  
    var last = model.last();
    var current = model.current();
  
    if (last.piece === current.piece) return;  
    if (!current.piece.next.isSwitch) return;
    if (Math.random() >= 0.5)         return;
    
    var maxLaneIndex = track.getMaxLaneIndex();
    var laneIndex    = current.lane;      
    
    if      (laneIndex === 0)            send({ msgType:"switchLane", data:"Right" });
    else if (laneIndex === maxLaneIndex) send({ msgType:"switchLane", data:"Left" });
    else if (Math.random() >= 0.5)       send({ msgType:"switchLane", data:"Right" });
    else                                 send({ msgType:"switchLane", data:"Left" });
  }
      
  module.exports = 
  {
    next: next,
    random: random
  };
    
})();






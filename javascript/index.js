var fs = require('fs');
var net = require('net');
var JSONStream = require('JSONStream');
var track = require('./track')
var newpos = require('./newpos')
var model = require('./model')
var switcher = require('./switcher')
var config = require('./config').getConfig();

var QUIET = true;

// arguments
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

// misc
var color;
var turboAvailable = false;

// messaging
var sendCount = 0;
var receiveCount = 0;
var messageLog = [];
var gameTick;

console.log(botName, serverHost, serverPort);

process.on('uncaughtException', function(e) 
{
  console.log('uncaughtException',e,e.stack);
}); 

client = net.connect(serverPort, serverHost, function() 
{
  console.log('connected');
  
  var track = config.track;
  if (track)
  {
      console.log('join '+track);
      return send(
      {
        "msgType": "joinRace", 
        "data":
        {
          "botId": { "name": botName, "key": botKey },
          "trackName": config.track,
          "carCount": config.carCount,
          "password": config.password
        }
      });
  }
  else
  {
    console.log('join default');
    return send({ msgType: "join", data: { name: botName, key: botKey } });
  }
  
});

client.on('end', function()
{
  console.log('client end');
  writeLog();
});

client.on('error', function(error)
{
  console.log('client error',error);
});

client.on('close', function()
{
  console.log('client close');
});

function send(json) 
{
  if (json.msgType === 'turbo')
  {
    if (turboAvailable) turboAvailable = false;
    else return;
  }  

  //json.gameTick = gameTick + 1;
  messageLog.push(json);
  var string = JSON.stringify(json);
  client.write(string);
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) 
{
  var type;
  try
  {
    messageLog.push(data);

    type = data.msgType;
    gameTick = data.gameTick;

    switch (type)
    {
      case 'carPositions': 
        carPositions(data);
        break;

      case 'gameInit':   
        track.init(data);
        model.init();     
        send({ msgType: 'ping', data: {} });
        break;
        
      case 'yourCar':
        color = data.data.color;        
        console.log('color',color);
        send({ msgType: 'ping', data: {} });
        break;
    
      case 'turboAvailable':
        turboAvailable = true;    
        console.log('--- turbo available ---');
        break;
        
      case 'crash':     
        var crashedColor = data.data.color;
        if (color === crashedColor)
        {   
          console.log('########################################  C R A S H  ########################################');        
          writeLog();
        }
        else
        {
          console.log('------ '+crashedColor+' CRASHED ------');
        }
        send({ msgType: 'ping', data: {} });
        break;
        
      case 'gameEnd':
        console.log(JSON.stringify(data));
        send({ msgType: 'ping', data: {} });
        writeLog();
        track.endLog();
        break;

      default: 
        console.log(JSON.stringify(data));
        send({ msgType: 'ping', data: {} });
        break;
    }
  }
  catch (e)
  {
    console.log('caught',e,e.stack);
  }
});

jsonStream.on('error', function() 
{
  console.log('error');
  return console.log('disconnected');
});

function writeLog()
{
//  if (__dirname.indexOf('/Users/michael') < 0) return;
//  var out = JSON.stringify(messageLog);
//  fs.writeFileSync('message_log_'+Date.now()+'.json', out);
//  fs.writeFileSync('message_log_last.json', out);
}

var slowdown = 
{
  slow:   0.92,
  medium: 0.95,
  fast:   0.98
};

function carPositions(data)
{
  model.next(data,color);

  var throttle;

  switch (config.behaviour)
  {
    case 'normal':
      switcher.next(send);
      throttle = newpos.next(send); 
      break;
      
    case 'slow': 
    case 'fast':   
      switcher.next(send);
      throttle = newpos.next(send) * slowdown[config.behaviour];
      break;

    case 'medium': 
      switcher.random(send);
      throttle = newpos.next(send) * slowdown[config.behaviour];
      break;      
  }

  if (throttle > 1) throttle = 1;
  model.setThrottle(throttle);
  model.log(messageLog);
  send({ msgType: "throttle", data: throttle });   
}

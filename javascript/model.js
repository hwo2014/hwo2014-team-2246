(function()
{
  console.log('model.<init>');
  
  var local = __filename.indexOf('/Users/michael/') > -1;

  // pre-computed track information
  var track = require('./track');
  var trackArray;
  
  // current and previous tick data
  var current = {};
  var last = {};
  var lastLast = {};
  
  // threshold for approximately zero
  var error = 0.01;
  
  // last turn for allocating car angle
  var lastTurn;
  
  // information about other cars by color
  var cars = {};
  
  // model constants
  var constants = 
  {
    A: 0,
    B: 0,
    Bsum: 0,
    Bcount: 0,
    //var C =  0.04244;
    //var D = -0.006314;
    //var E = -0.05834;
    //var F =  0.0942;
  };
  
  function init()
  {
    current = {};
    last = {};
    lastLast = {};
    lastTurn = null;
    cars = {};
  }
  
  function next(data,color)
  {
    if (!trackArray) trackArray = track.getTrackArray();
  
    var array = data.data;  
    for (var i=0; i<array.length; i++)
    {
      var car = array[i];
      if (car.id.color !== color) continue;
    
      var pieceIndex = car.piecePosition.pieceIndex;
      var piece      = trackArray[pieceIndex];
    
      current = 
      {
        tick     : data.gameTick,
        piece    : piece,
        lap      : car.piecePosition.lap,
        pos      : car.piecePosition.inPieceDistance,
        lane     : car.piecePosition.lane.startLaneIndex, 
        eLane    : car.piecePosition.lane.endLaneIndex,
        angle    : car.angle
      };
    
      current.switched = current.eLane !== current.lane;        
      
      current.duration = current.tick - (last.tick||0);      
      if (current.duration === 0) 
      {
        console.log("*** ERROR SAME TICK ? ***");
        continue;
      }
      
      var distance = current.pos - last.pos;
      if (last.piece && current.piece !== last.piece)
      {
        var pieceLength = last.piece.lane[last.lane].length; 
        distance += pieceLength;
      }
      if (last.lane !== current.lane)
      {
        // approximate over the end of a switch for smoothness
        distance = last.distance;
      }
      
      current.distance     = distance;
      current.speed        = current.distance/current.duration;
      current.acceleration = current.speed - last.speed;

      current.angularSpeed        = current.angle - last.angle;
      current.angularAcceleration = current.angularSpeed - last.angularSpeed;

      calcLinearConstants();
      calcAngularConstants();
      
      updateTurn();
      
      multiCarUpdate(data,color);

      lastLast = last;
      last = current;
    }
  }
  
  function multiCarUpdate(data,color)
  {
    var myLane        = current.lane;
    var myIndex       = current.piece.index;
    var myPieceLength = current.piece.lane[myLane].length
    var myPos         = current.pos;
    var mySpeed       = current.speed;
    var blocked       = false;
    
    var array = data.data;  
    for (var i=0; i<array.length; i++)
    {
      var car = array[i];
      var otherColor = car.id.color;
      if (otherColor === color) continue; // ignore me :-)
      
      var otherIndex = car.piecePosition.pieceIndex;
      var otherLane  = car.piecePosition.lane.startLaneIndex;
      
      // jut ignore errors around transition from max to zero index 
      // and switching lanes for now ...
      
      if      (otherLane !== myLane)    cars[otherColor] = { status:'o' };
      else if (otherIndex <  myIndex)   cars[otherColor] = { status:'-' };
      else if (otherIndex >  myIndex+1) cars[otherColor] = { status:'+' };
      else                              
      { 
        var otherPos = car.piecePosition.inPieceDistance;
        if (otherIndex > myIndex) otherPos += myPieceLength;
        var distance = otherPos - myPos;

        if (cars[otherColor])
        {
          var otherLastPos = cars[otherColor].pos;   
          if (otherLastPos)
          {
            var otherSpeed = otherPos - otherLastPos;
          
            // crude notion of blocked here is that they're just ahead of me
            // and I am going faster, however this would be wrong after a bump 
            // presumably or when actually blocked and slowed down etc
          
            if (mySpeed >= otherSpeed)
            {
              blocked = true;
            }          
          }
        }
        
        var status = Math.round(distance)+(blocked?'*':'');
        if (distance > 0) status = '+'+status;
        cars[otherColor] = { status:status, pos:otherPos };
      }
    }
    
    // my resultant status - available for switcher to use
    cars.blocked = blocked;
  }
  
  function updateTurn()
  {
    // don't push it in a real race!
    if (local || track.getNumLaps()) return;
  
    // update current turn with max car angle and speed if applicable    
    var turn = current.piece.turn || lastTurn;
    if (turn)
    {
      var lane = turn.lanes[current.lane];
      
      var a = Math.abs(current.angle);
      if (a > lane.actualAngle)
      {
        lane.actualAngle = a;
      }
      
      var s = current.speed;
      if (s > lane.actualSpeed)
      {
        lane.actualSpeed = s;
      }
    }
    
    // update last turn with new max speeds if applicable
    if (lastTurn && turn !== lastTurn)
    {
      var lane = lastTurn.lanes[current.lane];
    
      var angle  = lane.actualAngle;
      var actual = lane.actualSpeed;
      var speed  = lane.maxSpeed;
      var radius = lastTurn.minRadius;
      var factor;
      
      // only consider increasing max speed if actually used it already!
      if (actual > speed*0.98)
      {      
        if (radius <= 50)
        {
          if      (angle < 40) factor = 1.04;
          else if (angle < 45) factor = 1.02;
          else if (angle < 48) factor = 1.01;
          else if (angle < 52) factor = 1.005;
        }
        else
        {
          if      (angle < 40) factor = 1.055; 
          else if (angle < 45) factor = 1.035; 
          else if (angle < 48) factor = 1.025; 
          else if (angle < 52) factor = 1.015; 
        }
      
        if (factor)
        {
          lane.maxSpeed = speed * factor;
          console.log('++++  '+factor+' : '+speed+' -> '+lane.maxSpeed+'  ++++');
        }
        else
        {
          console.log('....  no increase  ....');
        }
      }
    }
    
    lastTurn = turn;
  }
  
  //----------------------------------------------------------------------------
  // acceleration = A*throttle - B*speed
  //----------------------------------------------------------------------------
  function calcLinearConstants()
  {      
      if (!last.speed)
      {
        constants.A = current.acceleration/last.throttle;
      }
      
      if (constants.A && 
          last.speed > 0 && 
          last.throttle > 0 &&
          !current .switched &&
          !last    .switched &&
          !lastLast.switched)
      {
        var c = (constants.A*last.throttle - current.acceleration)/last.speed;

        // ignore weird big deviations
        if (!constants.B || (c > constants.B*0.9 && c < constants.B*1.1))
        {
          constants.Bsum += c
          constants.Bcount++;
          constants.B = constants.Bsum / constants.Bcount;
        }
      }
  }

  function zero(n)
  {
    return (n > -error && n < error);
  }

  //----------------------------------------------------------------------------
  // acceleration = C*deltaTrackAngle - D*angle - E*angularSpeed;  
  //----------------------------------------------------------------------------
  function calcAngularConstants()
  {
    var pieceLength = current.piece.lane[current.lane].length; 
    
    var dta = pieceLength ? (current.piece.angle||0) * current.distance / pieceLength : 0
    
    if (!dta || isNaN(dta)) dta = 0;
    
    current.deltaTrackAngle = dta;
    
    /*
    var dta = last.deltaTrackAngle;
    
    var c = C*dta;
    var d = D*last.angle;
    var e = E*last.angularSpeed;

    // effectively some grip threshold ...
    if (Math.abs(c) < F) c = 0;
            
    current.c = c;
    current.d = d;
    current.e = e;   
    
    current.model = c + d + e;
    */
  }
    
  function setThrottle(throttle)
  {
    last.throttle = throttle;
  }

  function pad(s,n)
  {
    s = s===0 ? '0' : !s ? '' : ''+s;
    n -= s.length;
    if (n > 0) for (var i=0; i<n; i++) s = ' '+s;
    return s;
  }

  function fixed(a,b,n)
  {
    return pad(n.toFixed(a),b);
  }

  function num(n,a,b)
  {
    return pad(Math.round(n*a),b);
  }

  function log(messageLog)
  {
    var p = lastLast.piece === last.piece;
  
    messageLog.push(
    {
      log:              true,      
      tick:             last.tick,      
      duration:         last.duration,
      lap:              last.lap,
      pieceIndex:       last.piece.index,
      pieceRadius:      last.piece.radius,
      pieceAngle:       last.piece.angle,
      laneLength:       last.piece.lane[last.lane].length,
      laneIndex:        last.lane,
      laneIndexEnd:     last.eLane,
      throttle:         last.throttle,
      linearDist:       last.distance,
      linearSpeed:      last.speed,
      linearAccel:      last.acceleration,
      angularDist:      last.angle,
      angularSpeed:     last.angularSpeed,
      angularAccel:     last.angularAcceleration,
      deltaTrackAngle:  last.deltaTrackAngle,
      behaviour:        last.behaviour
    });
  
    var others = ''
    for (var color in cars)
    {
      var car = cars[color];
      if (car.status)
      {
        if (others.length) others += ' , ';
        others += pad(car.status,4);
      }
    }
  
    console.log(
    
      pad(last.tick,4),
      
      last.duration,

      'piece[',
      p ? ' '     : last.lap,
      p ? '  '    : pad(last.piece.index,2),
      p ? '   '   : num(last.piece.lane[last.lane].length, 1, 3),
      p ? '   '   : pad(last.piece.radius,3),
      p ? '     ' : pad(last.piece.angle,5),
      ']',
      
      'lane[',
      p ? ' ' : last.lane,
      p ? ' ' : last.eLane,
      ']',
      
      'gas', pad(Math.round(last.throttle*1000),4),
      
      // linear
      'lin[',
      num(last.distance,     100, 5),
      num(last.speed,        100, 5),
      num(last.acceleration, 100, 5),
      ']',
      
      // angular
      'ang[',      
      fixed(4,8, last.angle),
      fixed(4,8, last.angularSpeed),
      fixed(4,8, last.angularAcceleration),
      ']',
      
      'cars['+others+']',
      
      'const[',      
      constants.A.toFixed(2),
      constants.B.toFixed(2),
      ']',
      
      last.behaviour
      );
  }
  
  // returns throttle to get to maintain s
  // acceleration = A*throttle - B*speed;
  function getThrottleForSpeed(speed)
  {
    var throttle;
    
    if (constants.A && constants.B)
    {
      var acceleration = speed - last.speed;
      throttle = (acceleration + constants.B * last.speed) / constants.A;
    }
    else
    {
      throttle = 0.5;
    }
    
    if      (throttle < 0) throttle = 0;
    else if (throttle > 1) throttle = 1;   
    
    return throttle;
  }
  
  module.exports = 
  {
    init: init,
    next: next,
    getThrottleForSpeed: getThrottleForSpeed,
    setThrottle: setThrottle,
    log: log,
    current: function() { return current; }, // note after model.next current===last
    last: function() { return lastLast; },   // note after model.next lastLast is effectively last   
    constants: function() { return constants; },
    cars: function() { return cars; }
  };
  
})();

(function()
{ 
  console.log('track.<init>');

  var A = 2*Math.PI/360;

  var numLaps;  

  //
  // pieces
  //
  var trackArray; 
  var maxTrackIndex;

  //
  // lanes
  //
  var lanesArray;
  var numLanes;
 
  // 
  // segment : one or more pieces from start of switch to before next switch
  // 
  var segment = [ ]; 
  segment.index = 0;
  var segments = [ segment ];
  
  //
  // turn : one or more pieces of contiguous same direction turn
  //
  var turn;
  var turns = [];
  
  function addTurn(p)
  {
    if (!turn || turn.direction!==p.isTurn)
    {
      turn = { index:turns.length, direction:p.isTurn, angle:0, pieces:[], radiuses:[], minRadius:999, lanes:[] };
      turns.push(turn);
    }
    
    turn.pieces.push(p.index);
    turn.radiuses.push(p.radius);
    turn.angle += Math.abs(p.angle);
    if (p.radius < turn.minRadius) turn.minRadius = p.radius;
      
    p.turn = turn;
  }

  function addToSegment(laneIndex,length)
  {
    var s = segment[laneIndex] || 0;
    segment[laneIndex] = s + length;
  }
  
  function diagonal(a,b)
  {
    return Math.sqrt(Math.pow(a,2) + Math.pow(b,2));
  }

  function init(message)
  {
    console.log('track.init');
    
    // clear so re-init between qual and race works!    
    numLaps = null;
    trackArray = null;
    maxTrackIndex = null;
    lanesArray = null;
    numLanes = null;
    segment = [ ]; 
    segment.index = 0;
    segments = [ segment ];
    turn = null;
    turns = [];

    // clone so original logged message does not have the circular refs created later
    message = JSON.parse(JSON.stringify(message)); 

    // laps
    numLaps = message.data.race.raceSession.laps;

    // lanes
    lanesArray = message.data.race.track.lanes;
    numLanes = lanesArray.length;

    // pieces
    trackArray = message.data.race.track.pieces;
    maxTrackIndex = trackArray.length-1;
    processPieces();
        
    // post-process
    joinSegments();
    joinTurns();
    setTurnSpeeds();
    knownTracks(message.data.race.track.name);
    markStartOfFinalStraight();
        
    // log        
    logPieces();
    logTurns();
    logSegments();
    
    return trackArray;
  }

  function processPieces()
  {
    for (var trackIndex=0; trackIndex<=maxTrackIndex; trackIndex++)
    {      
      var t = trackArray[trackIndex];
      t.index   = trackIndex;
      t.lane    = [];
      t.segment = segment;
      t.prev    = trackIndex===0 ? trackArray[maxTrackIndex] : trackArray[trackIndex-1]; 
      t.next    = trackIndex===maxTrackIndex ? trackArray[0] : trackArray[trackIndex+1]; 
      
      t.isTurn = false;
      if (t.radius && t.angle)
      {              
        var isRight = t.angle > 0;
        var length  = A * Math.abs(t.angle);
        
        t.isTurn = isRight ? 'Right' : 'Left ';
        
        for (var laneIndex=0; laneIndex<numLanes; laneIndex++)
        {
          var d = lanesArray[laneIndex].distanceFromCenter;
          if (isRight) d = -d; 
          
          t.lane[laneIndex] = { length: length * (t.radius + d) };
        }
        
        addTurn(t);
      }
      else if (t.length)
      {
        turn = null;
        
        for (var laneIndex=0; laneIndex<numLanes; laneIndex++)
        {
          t.lane[laneIndex] = { length: t.length };
        }
      }
      
      t.isSwitch = false;
      if (t['switch'])
      {
        t.isSwitch = true;
        delete t['switch'];
        
        // allocate half the piece length to each segment 
        //
        // note: this is assuming the switch is not taken
        // so it's not quite accurate, potentially the 
        // "should I switch" calculation should use 
        // the otherLength+switch cost vs thisLength+noSwitch
        // although then it depends whether it has to switch
        // *back* as well. Hopefully this can be ignored
        // as too small a consequence in the decision process!
        
        for (var laneIndex=0; laneIndex<numLanes; laneIndex++)
        {
          addToSegment(laneIndex,t.lane[laneIndex].length/2);
        }
        
        t.segment = segment = [ ];
        segment.index = segments.length;
        segments.push(segment);
        
        for (var laneIndex=0; laneIndex<numLanes; laneIndex++)
        {
          addToSegment(laneIndex,t.lane[laneIndex].length/2);
        }
        
        // add lengths when switching lanes for use in calculating
        // actual distance travelled and thus speed at runtime
        
        for (var laneIndex=0; laneIndex<numLanes; laneIndex++)
        {
          // assumes a straight switch as a curved one is more complex ;-)
          
          var map = t.lane[laneIndex].switchTo = {};
          
          var length = t.lane[laneIndex].length;
          
          // left?
          if (laneIndex > 0)
          {
            var width = lanesArray[laneIndex  ].distanceFromCenter
                      - lanesArray[laneIndex-1].distanceFromCenter;
                      
            map.left = diagonal(length,width);
          }
          
          // right?
          if (laneIndex < numLanes-1)
          {
            var width = lanesArray[laneIndex+1].distanceFromCenter
                      - lanesArray[laneIndex  ].distanceFromCenter;
                      
            map.right = diagonal(length,width);
          }
        }
      }
      else
      {
        for (var laneIndex=0; laneIndex<numLanes; laneIndex++)
        {
          addToSegment(laneIndex,t.lane[laneIndex].length);
        }
      }
    }
  }

  /** Merge first and last segments. */
  function joinSegments()
  {
    var lastSegment = segments.pop();
    for (var laneIndex=0; laneIndex<numLanes; laneIndex++)
    {
      segments[0][laneIndex] += lastSegment[laneIndex];
    }
    for (var trackIndex=0; trackIndex<=maxTrackIndex; trackIndex++)
    {      
      var t = trackArray[trackIndex];
      if (t.segment === lastSegment) t.segment = segments[0];      
    }    
  }

  function joinTurns()
  {
    // TODO at the moment just assume piece[0] is not a turn!
  }

  function setTurnSpeeds()
  {
    for (var i=0; i<turns.length; i++)
    {
      var turn   = turns[i];
      var radius = turn.minRadius;
      var angle  = turn.angle;
      var s;
      
      if      (radius <=  50)               
      {
        if      (angle <= 22.5) s = 5.75;
        else if (angle <= 45  ) s = 5.50;
        else if (angle <= 67.5) s = 5.25;
        else if (angle <= 90  ) s = 5.00;
        else if (angle <= 135 ) s = 4.50; 
        else if (angle <= 180 ) s = 4.25; 
        else                    s = 4.00; 
      }      
      else if (radius <= 100) 
      {
        if      (angle <= 22.5) s = 7.75;
        else if (angle <= 45  ) s = 7.50;
        else if (angle <= 67.5) s = 7.25;
        else if (angle <= 90  ) s = 6.50;
        else if (angle <= 135 ) s = 6.50;
        else if (angle <= 180 ) s = 6.50;
        else                    s = 6.00;
      }
      else if (radius <= 200) 
      {        
        if      (angle <= 22.5) s = 9.75;
        else if (angle <= 45  ) s = 9.50;
        else if (angle <= 67.5) s = 9.25;
        else if (angle <= 90  ) s = 8.75; 
        else if (angle <= 135 ) s = 8.25; 
        else if (angle <= 180 ) s = 7.75; 
        else                    s = 7.25; 
      }
      else                                    
      {
        if      (angle <= 22.5) s = 10.0;
        else if (angle <= 45  ) s = 10.0;
        else if (angle <= 67.5) s = 10.0;
        else if (angle <= 90  ) s = 10.0;
        else if (angle <= 135 ) s = 9.75;
        else if (angle <= 180 ) s = 9.50;
        else                    s = 9.25;
      }
      
      for (var j=0; j<numLanes; j++)
      {
        turn.lanes[j] = { maxSpeed:s, actualSpeed:0, actualAngle:0 };
      }
    }
  }
  
  function setMaxSpeed(index, speed)
  {
    var lanes = turns[index].lanes;
    for (var i=0; i<lanes.length; i++)
    {
      lanes[i].maxSpeed = speed;
    }
  }
  
  /** Tweak turn defaults for known tracks. */
  function knownTracks(name)
  {
    switch (name)
    {
      case 'France':
        if (trackArray.length !== 44) return;
        console.log('====== FRANCE ======');
        turns[0].fastEntry = 1.7; 
        setMaxSpeed(1,6);
        setMaxSpeed(3,5); 
        setMaxSpeed(7,7.9); 
        turns[7].fastExit  = 1.7; 
        break;
        
      case 'Keimola':
        console.log('====== FINLAND ======');
        if (trackArray.length !== 40) return;
        setMaxSpeed(0,6.06); 
        setMaxSpeed(7,6.425);
        turns[0].fastEntry = 1.25;
        turns[2].fastEntry = 1.1;
        turns[2].fastExit  = 1.1;
        trackArray[8].isTurn = false; // power out early
        trackArray[8].turn = null;
        trackArray[23].isTurn = false; // power out early
        trackArray[23].turn = null;
        break;
    }
  }  

  function markStartOfFinalStraight()
  {
    var p = trackArray[0];
    while (!p.prev.isTurn) p = p.prev;
    p.isHomeStraight = true;
  }
  
  function logPieces()
  {
    console.log('PIECES');
    for (var trackIndex=0; trackIndex<=maxTrackIndex; trackIndex++)
    {      
      var t = trackArray[trackIndex];

      console.log('['+trackIndex+'] s'+t.segment.index+' '+
        (t.isTurn?t.isTurn:'     ')+' '+
        (t.isTurn?Math.round(Math.abs(t.angle)):'  ')+' '+
        (t.isSwitch?'x':' ')+
        ' lanes='+t.lane[0].length+','+t.lane[1].length+
        (t.isHomeStraight?' --- Home Straight ---':'')
        );
    }
  }

  /** Contiguous single-direction turn pieces. */
  function logTurns()
  {
    console.log('TURNS');
    for (var i=0; i<turns.length; i++)
    {
      var t = turns[i];
      console.log('['+i+'] '+JSON.stringify(t));
    }   
  }
  
  /** Contiguous pieces between switches, including the first switch. */
  function logSegments()
  {
    console.log('SEGMENTS');
    for (var segIndex=0; segIndex<segments.length; segIndex++)
    {
      var s = segments[segIndex];
      for (var laneIndex=0; laneIndex<numLanes; laneIndex++)
      {
        s[laneIndex] = s[laneIndex];
      }
      console.log('['+segIndex+'] '+JSON.stringify(s));
    }   
  } 

  function endLog()
  {
    logTurns();
  }

  module.exports = 
  {
    init: init,
    getTrackArray: function() { return trackArray; },
    getMaxLaneIndex: function() { return numLanes-1; },
    getNumLaps: function() { return numLaps; },
    endLog: endLog
  };

})();
